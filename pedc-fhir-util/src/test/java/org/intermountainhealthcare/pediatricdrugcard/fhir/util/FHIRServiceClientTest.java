package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.http.HttpStatus;
import org.hl7.fhir.instance.model.AtomFeed;
import org.hl7.fhir.instance.model.Observation;
import org.hl7.fhir.instance.model.Patient;
import org.junit.*;

import java.util.HashMap;
import java.util.Map;

public class FHIRServiceClientTest {

    private static FHIRServiceClient fhirClient = new FHIRServiceClient();

    @BeforeClass
    public static void initializeFHIRClient(){
        fhirClient.init("http://localhost:8080/open-hsp-api");
    }

    @Test
    public void testGetPatient() throws Exception{
        Patient patient = this.fhirClient.read(Patient.class, "828aeec4-87a6-4114-b7a2-e1456739f92c");
        Assert.assertNotNull(patient);

        try {
            patient = this.fhirClient.read(Patient.class, "unknown_id");
            Assert.assertTrue(false);
        }
        catch(FHIRServiceResponseException fsr_ex){
            Assert.assertEquals(HttpStatus.SC_NOT_FOUND, fsr_ex.getHttpStatusCode());
        }
        catch(Exception ex) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testSearchObservations() throws Exception{
        Map<String, String> searchParams = new HashMap<String, String>(){{
            put("subject", "828aeec4-87a6-4114-b7a2-e1456739f92c");
            put("name", "58941-6");
        }};

        AtomFeed feed = this.fhirClient.search(Observation.class, searchParams);
        Assert.assertNotNull(feed);
    }
}
