<%@ page import ="java.math.BigInteger" %>
<%@ page import ="java.math.RoundingMode" %>
<%@ page import ="java.sql.*" %>
<%@ page import ="javax.naming.*" %>
<%@ page import ="java.text.SimpleDateFormat" %>
<%@ page import ="java.text.NumberFormat" %>
<%@ page import ="java.text.DecimalFormat" %>
<%@ page import ="java.text.ParseException" %>
<%@ page import ="org.hl7.fhir.instance.client.FHIRSimpleClient" %>
<%@ page import ="org.hl7.fhir.instance.client.EFhirClientException"%>
<%@ page import ="org.hl7.fhir.instance.client.FeedFormat" %>
<%@ page import ="java.util.HashMap" %>
<%@ page import ="java.util.List" %>
<%@ page import="org.hl7.fhir.instance.client.ResourceFormat" %>
<%@ page import="org.intermountainhealthcare.pediatricdrugcard.fhir.util.FHIRClientUtil" %>
<%@ page import="org.intermountainhealthcare.pediatricdrugcard.fhir.util.PatientNameParts" %>
<%@ page import="org.hl7.fhir.instance.model.*" %>
<%@ page import="org.intermountainhealthcare.pediatricdrugcard.fhir.util.HeightAndWeightObs" %>
<%@ page import="java.math.BigDecimal" %>

<%!

    //public class example.FHIRClientExample {

    //    public static void main(String[] args) throws Exception{
    public void fhirClientExample ()
    {
        try
        {
            FHIRClientUtil clientUtil = new FHIRClientUtil();
            clientUtil.init("http://ihcutwas02/discern-fhir/fhir/p351");

            Patient patient = clientUtil.getPatient("1709937.0");
            PatientNameParts patientNameParts = clientUtil.getPatientNameParts(patient);

            List<String> givenNames = patientNameParts.getGivenNames();
            List<String> familyNames = patientNameParts.getFamilyNames();

            //Since there are is the possibility of multiple values here, the application will have to
            //decide on the right way to handle them. I understand that the pre-formatted name will be available though.
            //IMPORTANT: The middle name, if present, will be part of the given names list. We might have to consult with
            //Cerner
            if(givenNames.size() > 0){
                String givenNameSelection = givenNames.get(0);
                System.out.println(String.format("Patient Given Name: %s", givenNameSelection));
            }
            if(familyNames.size() > 0){
                String familyNameSelection = familyNames.get(0);
                System.out.println(String.format("Patient Family Name: %s", familyNameSelection));
            }
            System.out.println(String.format("Patient Formatted Name: %s", patientNameParts.getFormattedName()));

            DateAndTime patientBirthDate = clientUtil.getPatientBirthDate(patient);

            //Need to check for null here
            //You can get the month, day, year like you're doing it now
            if(patientBirthDate != null){
                System.out.println(String.format("Patient birth date: %d/%d/%d", patientBirthDate.getMonth(), patientBirthDate.getDay(), patientBirthDate.getYear()));
            }

            HeightAndWeightObs heightAndWeight = clientUtil.getLatestHeightAndWeightObs("1709937.0");

            Observation heightObs = heightAndWeight.getHeightObs();
            Observation weightObs = heightAndWeight.getWeightObs();

            //Need to check for null here. The application will have to decide what to do if one is missing
            //Here's how to round the value:
            if(heightObs != null){
                Quantity heightDecimalType = (Quantity)heightObs.getValue();
                BigDecimal heightValue = heightDecimalType.getValueSimple();
                heightValue = roundValue(heightValue);
                System.out.println(String.format("Latest Patient Height Obs: %s", heightValue));
            }
            if(weightObs != null){
                Quantity weightDecimalType = (Quantity)weightObs.getValue();
                BigDecimal weightValue = weightDecimalType.getValueSimple();
                weightValue = roundValue(weightValue);
                System.out.println(String.format("Latest Patient Weight Obs: %s", weightValue));
            }

            BigDecimal testValue = new BigDecimal("167.896");
            roundValue(testValue);
            testValue = new BigDecimal("05.2");
            roundValue(testValue);
            testValue = new BigDecimal("10.0");
            roundValue(testValue);
            testValue = new BigDecimal("12.30");
            roundValue(testValue);
            testValue = new BigDecimal(".01");
            roundValue(testValue);

        }

        catch(RuntimeException e)
        {
            e.printStackTrace();
        }

//  catch(EFhirClientException e)
//   {
//    System.out.println("**Error2**  " + e);
//   }


        catch( Exception e)
        {
            System.out.println("**Error**  " + e);
        }
        finally
        {
            System.out.println("did finally");
        }
    }

    public BigDecimal roundValue(BigDecimal value){
        BigDecimal roundedValue = value.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
        String roundedValueString = roundedValue.toPlainString();
        System.out.println(String.format("Original Value: %s, Rounded Value: %s", value, roundedValueString));
        return roundedValue;
    }

%>

<HTML>
<HEAD>
</HEAD>
<BODY>
<% fhirClientExample(); %>
</BODY>
</HTML>