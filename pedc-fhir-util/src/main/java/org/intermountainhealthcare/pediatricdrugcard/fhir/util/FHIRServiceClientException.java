package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

public class FHIRServiceClientException extends RuntimeException {
    public FHIRServiceClientException() {
    }

    public FHIRServiceClientException(String message) {
        super(message);
    }

    public FHIRServiceClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public FHIRServiceClientException(Throwable cause) {
        super(cause);
    }

    public FHIRServiceClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
