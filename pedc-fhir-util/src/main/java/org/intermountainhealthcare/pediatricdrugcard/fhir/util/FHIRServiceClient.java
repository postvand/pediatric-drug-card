package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.hl7.fhir.instance.formats.JsonParser;
import org.hl7.fhir.instance.formats.ResourceOrFeed;
import org.hl7.fhir.instance.model.AtomFeed;
import org.hl7.fhir.instance.model.Resource;
import org.hl7.fhir.instance.model.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FHIRServiceClient {
    private static final Logger log = LoggerFactory.getLogger(FHIRServiceClient.class);

    private static final String FHIR_RESPONSE_TYPE = "application/json+fhir";
    private static final String FHIR_RESPONSE_CHARSET = "UTF-8";
    private static final String FHIR_SERVICE_USER_AGENT = "Java FHIR Client for FHIR";

    private static final Map<String, String> HTTP_HEADERS = new HashMap<String, String>(){{
        put("User-Agent", FHIR_SERVICE_USER_AGENT);
        put("Accept", FHIR_RESPONSE_TYPE);
        put("Accept-Charset", FHIR_RESPONSE_CHARSET);
        put("Content-Type", String.format("%s;%s", FHIR_RESPONSE_TYPE, FHIR_RESPONSE_CHARSET));
    }};

    private String resourceURI;

    public void init(String resourceURI) throws FHIRServiceClientException {
        checkResourceURI(resourceURI);
        this.resourceURI = resourceURI;
    }

    public <T extends Resource> T read(Class<? extends Resource> resourceClass, String resourceId) throws FHIRServiceClientException{
        MethodUtil.require("resourceClass", resourceClass);
        MethodUtil.require("resourceId", resourceId);

        Resource resource = newResourceInstance(resourceClass);
        HttpResponse httpResponse = get(resource.getResourceType(), resourceId);
        return (T) processReadResponse(httpResponse);
    }

    protected AtomFeed search(Class<? extends Resource> resourceClass, Map<String, String> params) throws FHIRServiceClientException {
        MethodUtil.require("resourceClass", resourceClass);
        MethodUtil.require("params", params);

        Resource resource = newResourceInstance(resourceClass);
        HttpResponse httpResponse = get(resource.getResourceType(), params);
        return processSearchResponse(httpResponse);
    }

    protected HttpResponse get(ResourceType resourceType, Map<String, String> params) throws FHIRServiceClientException {
        HttpGet getRequest = new HttpGet(resolveSearchURI(resourceType, params));
        setRequestHeaders(getRequest);
        return sendRequest(getRequest);
    }

    protected HttpResponse get(ResourceType resourceType, String resourceId) throws FHIRServiceClientException {
        HttpGet getRequest = new HttpGet(resolveReadURI(resourceType, resourceId));
        setRequestHeaders(getRequest);
        return sendRequest(getRequest);
    }

    protected String resolveReadURI(ResourceType resourceType, String resourceId){
        return resolveReadURI(resourceType.name(), resourceId);
    }

    protected String resolveReadURI(String resourceType, String resourceId){
        String uriFormat = "%s/%s";
        String readURI = resolveResourceURI(resourceType);
        return String.format(uriFormat, readURI, resourceId);
    }

    protected String resolveSearchURI(ResourceType resourceType, Map<String, String> params){
        String searchURI = resolveResourceURI(resourceType.name());
        String paramFormat = "%s%s=%s";
        Set<String> paramNames = params.keySet();
        boolean isFirst = true;
        String separator = null;
        for(String paramName : paramNames){
            separator = isFirst ? "?" : "&";
            searchURI += String.format(paramFormat, separator, paramName, params.get(paramName));
            isFirst = false;
        }
        return searchURI;
    }

    protected String resolveResourceURI(String resourceType){
        String uriFormat = "%s/%s";
        return String.format(uriFormat, this.resourceURI, resourceType);
    }

    protected void setRequestHeaders(HttpRequest request){
        for(String headerKey : HTTP_HEADERS.keySet()){
            request.addHeader(headerKey, HTTP_HEADERS.get(headerKey));
        }
    }

    protected HttpResponse sendRequest(HttpUriRequest request) throws FHIRServiceClientException{
        HttpResponse response = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            response = httpclient.execute(request);
        }
        catch(IOException ioe) {
            String error = "An error occurred attempting to send the HTTP request";
            log.error(error, ioe);
            throw new FHIRServiceClientException(error, ioe);
        }
        return response;
    }

    protected Resource processReadResponse(HttpResponse httpResponse) throws FHIRServiceClientException{
        checkResponse(httpResponse);
        HttpEntity entity = httpResponse.getEntity();
        JsonParser parser = new JsonParser();
        Resource resource = null;

        try {
            resource = parser.parse(entity.getContent());
        }
        catch (Exception e) {
            String error = "An error occurred attempting to process the READ response.";
            log.error(error);
            throw new FHIRServiceClientException(error, e);
        }
        return resource;
    }

    protected AtomFeed processSearchResponse(HttpResponse httpResponse) throws FHIRServiceClientException{
        checkResponse(httpResponse);
        HttpEntity entity = httpResponse.getEntity();
        JsonParser parser = new JsonParser();
        ResourceOrFeed resourceOrFeed = null;
        try {
            resourceOrFeed = parser.parseGeneral(entity.getContent());
        }
        catch (Exception e) {
            String error = "An error occurred attempting to process the SEARCH response.";
            log.error(error);
            throw new FHIRServiceClientException(error, e);
        }
        return resourceOrFeed.getFeed();
    }

    private Resource newResourceInstance(Class<? extends Resource> resourceClass) throws FHIRServiceClientException{
        try {
            return resourceClass.newInstance();
        }
        catch (Exception ex) {
            throw new FHIRServiceClientException(ex);
        }
    }

    private void checkResourceURI(String resourceURI) throws FHIRServiceClientException {
        MethodUtil.require("resourceURI", resourceURI);
        try {
            new URI(resourceURI);
        }
        catch (URISyntaxException e) {
            throw new FHIRServiceClientException(String.format("'%s' is not a valid URI", resourceURI), e);
        }
    }

    private void checkResponse(HttpResponse httpResponse) throws FHIRServiceResponseException{
        int statusCode = httpResponse.getStatusLine().getStatusCode();

        if(HttpStatus.SC_OK != statusCode){
            String error = String.format("A FHIR service error occurred. Server returned HTTP Status Code = %d", statusCode);
            log.error(error);
            throw new FHIRServiceResponseException(statusCode, error);
        }
    }
}
