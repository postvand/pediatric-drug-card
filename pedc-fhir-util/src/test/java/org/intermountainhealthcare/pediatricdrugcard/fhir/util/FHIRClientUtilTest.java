package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.instance.model.DateAndTime;
import org.hl7.fhir.instance.model.HumanName;
import org.hl7.fhir.instance.model.Narrative;
import org.hl7.fhir.instance.model.Patient;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class FHIRClientUtilTest {

    @Test
    public void testFHIRClientUtilInit(){
        FHIRClientUtil fhirClientUtil = new FHIRClientUtil();

        //Test init
        String correctURL = "http://localhost:8080/test";
        String malformedURL = "This is a malformed URL";

        try {
            fhirClientUtil.init(correctURL);
        } catch (RuntimeException e) {
            Assert.assertTrue(false);
        }
        try {
            fhirClientUtil.init(malformedURL);
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }

        try {
            fhirClientUtil.init(malformedURL);
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }

        try {
            fhirClientUtil.init(null);
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }
    }

    //Just test basic stuff like expected exceptions, the rest
    //will have to be handled by integration tests
    @Test
    public void testGetPatient(){
        FHIRClientUtil fhirClientUtil = new FHIRClientUtil();
        try {
            fhirClientUtil.getPatient(null);
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }

        try {
            fhirClientUtil.getPatient("");
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void testGetLatestHeightWeightObs(){
        FHIRClientUtil fhirClientUtil = new FHIRClientUtil();
        try {
            fhirClientUtil.getLatestHeightAndWeightObs(null);
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }

        try {
            fhirClientUtil.getLatestHeightAndWeightObs("");
            Assert.assertTrue(false);
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void testGetPatientBirthDate(){

        //Test with null patient
        FHIRClientUtil fhirClientUtil = new FHIRClientUtil();
        try {
            fhirClientUtil.getPatientBirthDate(null);
            Assert.assertTrue(false);
        } catch (IllegalArgumentException e) {

        }

        //Test no birth date
        Patient patient = new Patient();
        DateAndTime patientBirthDate = fhirClientUtil.getPatientBirthDate(patient);
        Assert.assertNull(patientBirthDate);

        //Test with valid birth date
        Date now = new Date();
        DateAndTime nowDateAndTime = new DateAndTime(now);
        patient.setBirthDateSimple(nowDateAndTime);

        patientBirthDate = fhirClientUtil.getPatientBirthDate(patient);
        Assert.assertEquals(nowDateAndTime, patientBirthDate);
    }

    @Test
    public void testGetPatientNameParts(){
        //Test with null patient
        FHIRClientUtil fhirClientUtil = new FHIRClientUtil();
        try {
            fhirClientUtil.getPatientNameParts(null);
            Assert.assertTrue(false);
        } catch (IllegalArgumentException e) {

        }

        //Test with no names
        Patient patient = new Patient();
        PatientNameParts patientNameParts = fhirClientUtil.getPatientNameParts(patient);
        Assert.assertNotNull(patientNameParts);
        List<String> givenNames = patientNameParts.getGivenNames();
        List<String> familyNames = patientNameParts.getFamilyNames();
        String formattedName = patientNameParts.getFormattedName();

        Assert.assertNotNull(givenNames);
        Assert.assertEquals(0, givenNames.size());

        Assert.assertNotNull(familyNames);
        Assert.assertEquals(0, familyNames.size());

        Assert.assertTrue(StringUtils.isEmpty(formattedName));

        //Just a family name
        HumanName patientHumanName = patient.addName();
        patientHumanName.addFamilySimple("TestFamilyName1");
        patientHumanName.addFamilySimple("TestFamilyName2");

        patientNameParts = fhirClientUtil.getPatientNameParts(patient);
        Assert.assertNotNull(patientNameParts);
        givenNames = patientNameParts.getGivenNames();
        familyNames = patientNameParts.getFamilyNames();
        formattedName = patientNameParts.getFormattedName();

        Assert.assertEquals(0, givenNames.size());
        Assert.assertEquals(2, familyNames.size());
        Assert.assertTrue(StringUtils.isEmpty(formattedName));

        Assert.assertTrue(familyNames.contains("TestFamilyName1"));
        Assert.assertTrue(familyNames.contains("TestFamilyName2"));

        //Test Formatted Name
        Narrative patientNameNararative = new Narrative();
        XhtmlNode narrativeNode = new XhtmlNode(NodeType.Document);
        patientNameNararative.setDiv(narrativeNode);
        patient.setText(patientNameNararative);

        patientNameParts = fhirClientUtil.getPatientNameParts(patient);
        Assert.assertNotNull(patientNameParts.getFormattedName());
    }
}
