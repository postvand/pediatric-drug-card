package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

public class UnexpectedResultSizeException extends RuntimeException {
    private int expected;
    private int actual;

    public UnexpectedResultSizeException(int expected, int actual, String message) {
        super(message);
        this.expected = expected;
        this.actual = actual;
    }

    public int getExpected() {
        return expected;
    }

    public int getActual() {
        return actual;
    }
}
