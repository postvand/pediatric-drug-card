package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PatientNameParts {
    private List<String> givenNames = new ArrayList<String>();
    private List<String> familyNames = new ArrayList<String>();
    private String formattedName = "";

    public List<String> getGivenNames() {
        return givenNames;
    }

    public List<String> getFamilyNames() {
        return familyNames;
    }

    public String getFormattedName() {
        return formattedName;
    }

    public void addGivenName(String givenName){
        this.givenNames.add(givenName);
    }

    public void addFamilyName(String familyName){
        this.familyNames.add(familyName);
    }

    public void setFormattedName(String formattedName){
        if(StringUtils.isNotBlank(formattedName)){
            this.formattedName = formattedName;
        }
    }
}
