package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.hl7.fhir.instance.model.Observation;

public class HeightAndWeightObs {
    private Observation heightObs = null;
    private Observation weightObs = null;

    public HeightAndWeightObs(Observation heightObs, Observation weightObs){
        this.heightObs = heightObs;
        this.weightObs = weightObs;
    }

    public Observation getHeightObs() {
        return heightObs;
    }

    public Observation getWeightObs() {
        return weightObs;
    }
}
