package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

public class FHIRServiceResponseException extends FHIRServiceClientException {
    private int httpStatusCode;

    public FHIRServiceResponseException(int httpStatusCode, String message, Throwable cause) {
        super(message, cause);
        this.httpStatusCode = httpStatusCode;
    }

    public FHIRServiceResponseException(int httpStatusCode, String message) {
        super(message);
        this.httpStatusCode = httpStatusCode;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

}
