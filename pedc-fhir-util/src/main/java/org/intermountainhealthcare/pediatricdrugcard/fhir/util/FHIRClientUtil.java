package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.instance.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public class FHIRClientUtil {

    private static final Logger log = LoggerFactory.getLogger(FHIRClientUtil.class);

    private static final String HEIGHT_OBS_LOINC_CODE = "8302-2";
    private static final String WEIGHT_OBS_LOINC_CODE = "3141-9";

    FHIRServiceClient fhirClient = null;

    public void init(String baseServiceURL) throws FHIRServiceClientException {
        fhirClient = new FHIRServiceClient();
        try {
            fhirClient.init(baseServiceURL);
        }
        catch (Exception ex) {
            log.error("There was a syntax problem with the baseServiceURL", ex);
            throw new RuntimeException(ex);
        }
    }

    public Patient getPatient(String patientId) throws FHIRServiceClientException{
        if(StringUtils.isBlank(patientId)){
            log.error("Patient identifier is null or empty.");
            throw new IllegalArgumentException("Patient identifier is null or empty.");
        }
        return fhirClient.read(Patient.class, patientId);
    }

    public HeightAndWeightObs getLatestHeightAndWeightObs(final String patientId) throws FHIRServiceClientException{
        if(StringUtils.isBlank(patientId)){
            log.error("Patient identifier is null or empty.");
            throw new IllegalArgumentException("Patient identifier is null or empty.");
        }
        HashMap<String, String> obsSearchParams = new HashMap<String, String>(){{
            put("subject:Patient", patientId);
            put("name", HEIGHT_OBS_LOINC_CODE);
            put("_count", "1");
        }};
        Observation heightObs = getSingleObservationResult(this.fhirClient.search(Observation.class, obsSearchParams), HEIGHT_OBS_LOINC_CODE);

        obsSearchParams.put("name", WEIGHT_OBS_LOINC_CODE);
        Observation weightObs = getSingleObservationResult(this.fhirClient.search(Observation.class, obsSearchParams), WEIGHT_OBS_LOINC_CODE);

        return new HeightAndWeightObs(heightObs, weightObs);
    }

    public DateAndTime getPatientBirthDate(Patient patient) throws IllegalArgumentException {
        if(patient == null){
            throw new IllegalArgumentException("Argument 'patient' cannot be null");
        }
        DateTimeType birthDateTime = patient.getBirthDate();
        DateAndTime birthDateAndTime = null;
        if(birthDateTime != null){
            birthDateAndTime = birthDateTime.getValue();
        }
        return birthDateAndTime;
    }

    public PatientNameParts getPatientNameParts(Patient patient) throws IllegalArgumentException {
        if(patient == null){
            throw new IllegalArgumentException("Argument 'patient' cannot be null");
        }
        PatientNameParts nameParts = new PatientNameParts();
        List<HumanName> patientNames = patient.getName();
        if(patientNames != null){

            if(patientNames.size() > 0){
                //Cerner should only be sending one name
                //TODO: Map this to "usual" name-use code?
                HumanName patientName = patientNames.get(0);
                StringType patientNameText = patientName.getText();
                if(patientNameText != null){
                    nameParts.setFormattedName(patientNameText.asStringValue());
                }
                for(StringType givenName : patientName.getGiven()){
                    nameParts.addGivenName(givenName.asStringValue());
                }
                for(StringType familyName : patientName.getFamily()){
                    nameParts.addFamilyName(familyName.asStringValue());
                }
            }
        }
        return nameParts;
    }

    private Observation getSingleObservationResult(AtomFeed obsFeed, String obsCode) throws UnexpectedResultSizeException {
        Observation obs = null;
        String error = "";
        if(obsFeed == null){
            error = String.format("Null results feed for Obervations with code:'%s'", obsCode);
            log.error(error);
            throw new RuntimeException(error);
        }
        List entryList = obsFeed.getEntryList();
        if(entryList != null){
            int numResults = entryList.size();
            if(numResults == 1){
                obs = (Observation)obsFeed.getEntryList().get(0).getResource();
            }
            else if(numResults > 1){
                error = String.format("Unexpected Observation results size for Observations with code:'%s'. Expected:'%d', Actual: '%d'", obsCode, 1, numResults);
                log.error(error);
                throw new UnexpectedResultSizeException(1, numResults, error);
            }
        }
        return obs;
    }


}
