package org.intermountainhealthcare.pediatricdrugcard.fhir.util;

import org.apache.commons.lang.StringUtils;

public class MethodUtil {
    public static void require(String argName, Object argValue){
        if(argValue == null){
            throw new IllegalArgumentException(String.format("Argument '%s' cannot be null", argName));
        }
    }

    public static void require(String argName, String argValue){
        if(StringUtils.isBlank(argValue)){
            throw new IllegalArgumentException(String.format("Argument '%s' cannot be null or empty", argName));
        }
    }

}
